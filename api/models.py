from django.contrib.auth.models import User
from django.db import models
from django.template.defaultfilters import slugify

class Restaurant(models.Model):
    name = models.CharField(max_length=100, unique=True)
    slug = models.SlugField()

    def check_slug(self):
        if not self.id:
            self.slug = slugify(self.name)

    def clean(self):
        self.check_slug()

    def save(self, *args, **kwargs):
        self.check_slug()
        super(Restaurant, self).save(*args, **kwargs)

    def get_likes(self):
        return RestaurantLike.objects.filter(restaurant=self).count()

    def get_comments_amount(self):
        return RestaurantComment.objects.filter(restaurant=self).count()

class RestaurantLike(models.Model):
    user = models.ForeignKey(User, related_name='likes')
    restaurant = models.ForeignKey(Restaurant, related_name='likes')

    class Meta:
        unique_together = ('user', 'restaurant')

class RestaurantComment(models.Model):
    user = models.ForeignKey(User, related_name='comments')
    restaurant = models.ForeignKey(Restaurant, related_name='comments')
    created_at = models.DateTimeField(auto_now_add=True)
    body = models.TextField()

    class Meta:
        ordering = ('created_at',)
