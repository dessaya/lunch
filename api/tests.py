from rest_framework.test import APIClient
from django.test import TestCase
from contextlib import contextmanager
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from models import Restaurant

@contextmanager
def login(**kwargs):
    client = APIClient()
    assert client.login(**kwargs)
    yield client
    client.logout()

def url_restaurant_like(*args):
    return reverse('restaurant-like', args=args)

class UserTest(TestCase):
    required_data = {
        'username': 'john',
        'password': 'secret',
    }

    def test_register_incomplete_data(self):
        for k, v in UserTest.required_data.iteritems():
            incomplete_data = {k2: v2 for k2, v2 in UserTest.required_data.iteritems() if k2 != k}
            response = APIClient().post(reverse('user-list'), incomplete_data)
            self.assertEquals(400, response.status_code)
        with self.assertRaises(User.DoesNotExist):
            User.objects.get(username=UserTest.required_data['username'])

    def test_register_ok(self):
        response = APIClient().post(reverse('user-list'), UserTest.required_data)
        self.assertEquals(201, response.status_code)

        user =  User.objects.get(username=UserTest.required_data['username'])
        self.assertIsNotNone(user)
        self.assertEquals(user.username, UserTest.required_data['username'])
        self.assertTrue(user.check_password(UserTest.required_data['password']))

class RestaurantTest(TestCase):
    def setUp(self):
        User.objects.create_user(username='john', password='secret')
        User.objects.create_user(username='jane', password='secret')
        Restaurant.objects.create(name='sasha')

    def test_0_likes(self):
        self.assertEquals(0, Restaurant.objects.get(name='sasha').get_likes())

    def test_like_anonymous_unauthorized(self):
        response = APIClient().post(reverse('restaurant-like', args=('sasha',)))
        self.assertEquals(403, response.status_code)

    def test_like_once(self):
        with login(username='john', password='secret') as client:
            response = client.post(reverse('restaurant-like', args=('sasha',)))
            self.assertEquals(201, response.status_code)

        self.assertEquals(1, Restaurant.objects.get(name='sasha').get_likes())

    def test_like_twice_not_allowed(self):
        with login(username='john', password='secret') as client:
            response = client.post(reverse('restaurant-like', args=('sasha',)))
            self.assertEquals(201, response.status_code)
            response = client.post(reverse('restaurant-like', args=('sasha',)))
            self.assertEquals(409, response.status_code)

        self.assertEquals(1, Restaurant.objects.get(name='sasha').get_likes())

    def test_like_from_two_users(self):
        with login(username='john', password='secret') as client:
            response = client.post(reverse('restaurant-like', args=('sasha',)))
            self.assertEquals(201, response.status_code)

        with login(username='jane', password='secret') as client:
            response = client.post(reverse('restaurant-like', args=('sasha',)))
            self.assertEquals(201, response.status_code)

        self.assertEquals(2, Restaurant.objects.get(name='sasha').get_likes())

    def test_unlike_not_allowed(self):
        with login(username='john', password='secret') as client:
            response = client.delete(reverse('restaurant-like', args=('sasha',)))
            self.assertEquals(404, response.status_code)

        self.assertEquals(0, Restaurant.objects.get(name='sasha').get_likes())

    def test_unlike(self):
        with login(username='john', password='secret') as client:
            response = client.post(reverse('restaurant-like', args=('sasha',)))
            self.assertEquals(201, response.status_code)
            response = client.delete(reverse('restaurant-like', args=('sasha',)))
            self.assertEquals(200, response.status_code)

        self.assertEquals(0, Restaurant.objects.get(name='sasha').get_likes())

class RestaurantCommentTest(TestCase):
    def setUp(self):
        User.objects.create_user(username='john', password='secret')
        User.objects.create_user(username='jane', password='secret')
        Restaurant.objects.create(name='sasha')

    def test_comment_anonymous_unauthorized(self):
        response = APIClient().post(reverse('restaurant-comment', args=('sasha',)), {'body': 'hello'})
        self.assertEquals(403, response.status_code)

    def test_comment(self):
        with login(username='john', password='secret') as client:
            response = client.post(reverse('restaurant-comment', args=('sasha',)), {'body': 'hello'})
            self.assertEquals(201, response.status_code)
