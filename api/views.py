from django.contrib.auth.models import User
from django.db import transaction, IntegrityError
from django.db.models import Count
from django.shortcuts import get_object_or_404
from serializers import *
from rest_framework import viewsets, status
from rest_framework.decorators import permission_classes, detail_route
from rest_framework.permissions import AllowAny, IsAdminUser
from permissions import IsStaffOrTargetUser
from rest_framework.response import Response
from models import Restaurant, RestaurantLike, RestaurantComment

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    lookup_field = 'username'

    def get_permissions(self):
        if self.request.method == 'DELETE':
            return [IsAdminUser()]
        elif self.request.method == 'POST':
            return [AllowAny()]
        else:
            return [IsStaffOrTargetUser()]

class RestaurantViewSet(viewsets.ModelViewSet):
    queryset = Restaurant.objects.annotate(likes_amount=Count('likes')).order_by('-likes_amount')
    lookup_field = 'slug'

    def get_serializer_class(self):
        # Show a summary for each Restaurant in list view:
        if self.action == 'list':
            return RestaurantSummarySerializer

        # Hack: ViewSet class does not allow to easily configure
        # different serializers for custom actions:
        if 'like' in self.request.path_info:
            return RestaurantLikeSerializer
        if 'comment' in self.request.path_info:
            return RestaurantCommentCreateSerializer

        return RestaurantSerializer

    @detail_route(methods=['post', 'delete'])
    def like(self, request, slug=None):
        if request.method == 'POST':
            try:
                with transaction.atomic():
                    RestaurantLike.objects.create(user=request.user, restaurant=self.get_object()).save()
            except IntegrityError:
                return Response({'error': 'Already Liked'}, status=status.HTTP_409_CONFLICT)
            return Response(status=status.HTTP_201_CREATED)
        else:
            get_object_or_404(RestaurantLike, user=request.user, restaurant=self.get_object()).delete()
            return Response()

    @detail_route(methods=['post'])
    def comment(self, request, slug=None):
        RestaurantComment.objects.create(
            user=request.user,
            restaurant=self.get_object(),
            body=request.DATA['body'],
        ).save()
        return Response(status=status.HTTP_201_CREATED)
