from django.contrib.auth.models import User
from rest_framework import serializers
from models import Restaurant, RestaurantComment

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'password')
        lookup_field = 'username'
        write_only_fields = ('password', 'email')

    def restore_object(self, attrs, instance=None):
        user = super(UserSerializer, self).restore_object(attrs, instance)
        user.set_password(attrs['password'])
        return user

class RestaurantCommentSerializer(serializers.ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = RestaurantComment
        fields = ('user', 'created_at', 'body')
        read_only_fields = ('created_at',)

class RestaurantBaseSerializer(serializers.HyperlinkedModelSerializer):
    likes = serializers.IntegerField(source='get_likes', read_only=True)
    action_like = serializers.HyperlinkedIdentityField(view_name='restaurant-like')
    comments_amount = serializers.IntegerField(source='get_comments_amount', read_only=True)

    class Meta:
        model = Restaurant
        lookup_field = 'slug'

class RestaurantSummarySerializer(RestaurantBaseSerializer):
    class Meta(RestaurantBaseSerializer.Meta):
        fields = ('url', 'action_like', 'name', 'likes', 'comments_amount')

class RestaurantSerializer(RestaurantBaseSerializer):
    action_comment = serializers.HyperlinkedIdentityField(view_name='restaurant-comment')
    comments = RestaurantCommentSerializer(many=True, read_only=True)

    class Meta(RestaurantBaseSerializer.Meta):
        fields = ('url', 'action_like', 'action_comment', 'name', 'likes', 'comments_amount', 'comments')

class RestaurantLikeSerializer(serializers.Serializer):
    pass

class RestaurantCommentCreateSerializer(serializers.Serializer):
    body = serializers.CharField()
