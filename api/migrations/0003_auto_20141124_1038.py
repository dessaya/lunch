# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0002_auto_20141123_2258'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='RestaurantVote',
            new_name='RestaurantLike',
        ),
    ]
